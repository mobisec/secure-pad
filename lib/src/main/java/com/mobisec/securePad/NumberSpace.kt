package com.mobisec.securePad

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.*
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout

/**
 * Number keyboard (to enter pin or custom amount).
 */
@Suppress("MemberVisibilityCanBePrivate", "unused")
class NumberSpace : ConstraintLayout {

    private var securepad_pinLength: Int = 6
    private var securepad_pinAutoReturn: Boolean = false

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeAttributes(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initializeAttributes(attrs)
    }


    /**
     * Initializes XML attributes.
     */
    private fun initializeAttributes(attrs: AttributeSet?) {
        val array = context.theme.obtainStyledAttributes(attrs, R.styleable.securePad, 0, 0)
        try {
            // Secure fallback
            securepad_pinAutoReturn = false
            // Get key sizes
            securepad_pinLength = array.getInteger(R.styleable.securePad_securepad_pinLength, DEFAULT_LENGTH_DP)
            securepad_pinAutoReturn = array.getBoolean(R.styleable.securePad_securepad_pinAutoReturn, false)
        } finally {
            array.recycle()
        }
    }

    companion object {

        private const val DEFAULT_LENGTH_DP = 6

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}
