package com.mobisec.securePad

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorRes
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import java.util.*

/**
 * Number keyboard (to enter pin or custom amount).
 */
@Suppress("MemberVisibilityCanBePrivate", "unused")
class NumberKeyboard : ConstraintLayout {

    @Dimension
    private var keySize: Int = 0
    @Dimension
    private var keyPadding: Int = 0
    @DrawableRes
    private var numberKeyBackground: Int = 0
    @ColorRes
    private var numberKeyTextColor: Int = 0
    @DrawableRes
    private var leftAuxBtnIcon: Int = 0
    @DrawableRes
    private var leftAuxBtnBackground: Int = 0
    @DrawableRes
    private var rightAuxBtnIcon: Int = 0
    @DrawableRes
    private var rightAuxBtnBackground: Int = 0

    private lateinit var numericKeys: MutableList<TextView>
    private lateinit var leftAuxBtn: ImageView
    private lateinit var rightAuxBtn: ImageView

    private var listener: NumberKeyboardListener? = null

    constructor(context: Context) : super(context) {
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initializeAttributes(attrs)
        inflateView()
    }

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initializeAttributes(attrs)
        inflateView()
    }

    /**
     * Sets keyboard listener.
     */
    fun setListener(listener: NumberKeyboardListener?) {
        this.listener = listener
    }

    /**
     * Sets key width in px.
     */
    fun setKeySize(px: Int) {
        if (px == DEFAULT_KEY_SIZE_DP) return
        for (key in numericKeys) {
            key.layoutParams.width = px
            key.layoutParams.height = px
        }
        leftAuxBtn.layoutParams.width = px
        leftAuxBtn.layoutParams.height = px
        rightAuxBtn.layoutParams.width = px
        rightAuxBtn.layoutParams.height = px
        requestLayout()
    }

    /**
     * Sets key padding in px.
     */
    fun setKeyPadding(px: Int) {
        for (key in numericKeys) {
            key.setPadding(px, px, px, px)
            key.compoundDrawablePadding = -1 * px
        }
        leftAuxBtn.setPadding(px, px, px, px)
        rightAuxBtn.setPadding(px, px, px, px)
    }

    /**
     * Sets number keys background.
     */
    fun setNumberKeyBackground(@DrawableRes background: Int) {
        for (key in numericKeys) {
            key.background = ContextCompat.getDrawable(context, background)
        }
        rightAuxBtn.background = ContextCompat.getDrawable(context, background)
        leftAuxBtn.background = ContextCompat.getDrawable(context, background)
    }

    /**
     * Sets number keys text color.
     */
    fun setNumberKeyTextColor(@ColorRes color: Int) {
        for (key in numericKeys) {
            key.setTextColor(ContextCompat.getColorStateList(context, color))
        }
    }

    /**
     * Sets number keys text typeface.
     */
    fun setNumberKeyTypeface(typeface: Typeface) {
        for (key in numericKeys) {
            key.typeface = typeface
        }
    }

    /**
     * Sets left auxiliary button icon.
     */
    fun setLeftAuxButtonIcon(@DrawableRes icon: Int) {
        leftAuxBtn.setImageResource(icon)
    }

    /**
     * Sets right auxiliary button icon.
     */
    fun setRightAuxButtonIcon(@DrawableRes icon: Int) {
        rightAuxBtn.setImageResource(icon)
    }

    /**
     * Initializes XML attributes.
     */
    private fun initializeAttributes(attrs: AttributeSet?) {
        val array = context.theme.obtainStyledAttributes(attrs, R.styleable.securePad, 0, 0)
        try {
            // Get keyboard type
            val fingerprintPad = array.getBoolean(R.styleable.securePad_securepad_enableFingerprint, false)
            // Get key sizes
            keySize = array.getLayoutDimension(R.styleable.securePad_securepad_keySize, DEFAULT_KEY_SIZE_DP)
            // Get key padding
            keyPadding = array.getDimensionPixelSize(R.styleable.securePad_securepad_keyPadding,
                    dpToPx(DEFAULT_KEY_PADDING_DP.toFloat()))
            // Get number key background
            numberKeyBackground = array.getResourceId(R.styleable.securePad_securepad_numberKeyBackground,
                    R.drawable.securepad_key_bg)
            // Get number key text color
            numberKeyTextColor = array.getResourceId(R.styleable.securePad_securepad_numberKeyTextColor,
                    R.drawable.securepad_key_text_color)
            // Get auxiliary icons
            if (fingerprintPad) { // show fingerprint key
                leftAuxBtnIcon = R.drawable.securepad_ic_backspace
                rightAuxBtnIcon = R.drawable.securepad_key_bg_transparent
                leftAuxBtnBackground = R.drawable.securepad_key_bg_transparent
                rightAuxBtnBackground = R.drawable.securepad_key_bg_transparent
            } else {
                leftAuxBtnIcon = R.drawable.securepad_ic_backspace
                rightAuxBtnIcon = R.drawable.securepad_ic_fingerprint
                leftAuxBtnBackground = R.drawable.securepad_key_bg_transparent
                rightAuxBtnBackground = R.drawable.securepad_key_bg_transparent
            }
        } finally {
            array.recycle()
        }
    }

    /**
     * Inflates layout.
     */
    private fun inflateView() {
        val view = View.inflate(context, R.layout.securepad_layout, this)
        // Get numeric keys
        numericKeys = ArrayList(10)
        numericKeys.add(view.findViewById(R.id.key0))
        numericKeys.add(view.findViewById(R.id.key1))
        numericKeys.add(view.findViewById(R.id.key2))
        numericKeys.add(view.findViewById(R.id.key3))
        numericKeys.add(view.findViewById(R.id.key4))
        numericKeys.add(view.findViewById(R.id.key5))
        numericKeys.add(view.findViewById(R.id.key6))
        numericKeys.add(view.findViewById(R.id.key7))
        numericKeys.add(view.findViewById(R.id.key8))
        numericKeys.add(view.findViewById(R.id.key9))
        // Get auxiliary keys
        leftAuxBtn = view.findViewById(R.id.leftAuxBtn)
        rightAuxBtn = view.findViewById(R.id.rightAuxBtn)
        // Set styles
        setStyles()
        // Set listeners
        setupListeners()
    }

    /**
     * Set styles.
     */
    private fun setStyles() {
        setKeySize(keySize)
        setKeyPadding(keyPadding)
        setNumberKeyBackground(numberKeyBackground)
        setNumberKeyTextColor(numberKeyTextColor)
        setLeftAuxButtonIcon(leftAuxBtnIcon)
        setRightAuxButtonIcon(rightAuxBtnIcon)
    }

    /**
     * Setup on click listeners.
     */
    private fun setupListeners() {
        // Set number callbacks
        for (i in numericKeys.indices) {
            val key = numericKeys[i]
            key.setOnClickListener {
                listener?.onNumberClicked(i)
            }
        }
        // Set auxiliary key callbacks
        leftAuxBtn.setOnClickListener {
            listener?.onLeftAuxButtonClicked()
        }
        rightAuxBtn.setOnClickListener {
            listener?.onRightAuxButtonClicked()
        }
    }

    /**
     * Utility method to convert dp to pixels.
     */
    fun dpToPx(valueInDp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, resources.displayMetrics).toInt()
    }

    companion object {

        private const val DEFAULT_KEY_SIZE_DP = -1 // match_parent
        private const val DEFAULT_KEY_PADDING_DP = 16

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }
}
